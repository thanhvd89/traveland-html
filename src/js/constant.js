export const cities = [
  {
    id: 1,
    name: "Hanoi",
    description:
      "Known as a progressive city, Hanoi is now possessing from basic to world-class ranked hotels, good restaurants with top diverse cuisines and the best multi-lingual tour guiding team in Southeast Asia. Despite the rapid growth of modernization, the old charm of a thousand-year town is still well-maintained and harmonized with latter-day buildings and sparkling arcades. Most visitors in Hanoi will prefer, though, to stay in the complicated Old Quarter in order to walk over its exciting guilds-streets, to taste its incredibly delicious traditional foods and to stroll around long poetic roads or lakes. More and more people take Hanoi as a ‘’hub’’ for multiple diversified day tours and perhaps more than any cities of Indochina.",
    info_1:
      "Historical & cultural sites: Ho Chi Minh Complex (Mausoleum, Museum, One Pillar Pagoda, House-on-stilts), Temple of Literature, Hoa Lo “Hanoi Hilton” Prison.",
    info_2: "Temples: Ngoc Son Temple, Tran Quoc Pagoda, Quan Thanh temple.",
    info_3:
      "Museums: National Museum of History, Vietnam Museum of Ethnology, Fine Art, Vietnamese Women's Museum.",
    info_4:
      "Activities: Cyclo/electric car/walking tour in Old Quarter; street food, coffee tasting, Bia hoi (local beer) at Ta Hien street, joining the locals for morning exercise in Hoan Kiem Lake.",
  },
  {
    id: 2,
    name: "Ha Long",
    description:
      "Your trip to Vietnam is not complete without an overnight cruise voyage to one of the amazing Halong bay, Bai Tu Long, or Lan Ha Bays. These three bays are similar in many ways despite sharing the same vast area and similar geological, geographical, and climatic characteristics. Halong Bay holds a central position with an impressive cave system. Bai Tu Long Bay is located in the northeast, while Lan Ha Bay is hidden from the crowd in the southwest with much fewer boats. Due to its geological value and graceful beauty, Halong Bay has earned the title of One of the New 7 Natural World Wonders and has seen a significant increase in tourists in recent years.",
    info_1:
      "Halong bay attraction: Thien Cung, Dau Go, Me Cung, Sung Sot, Trong caves and hidden tunnel caves Ba Hang and Luon; Ba Hang, Hoa Cuong, and Cua Van floating villages; Ti Top Island and Soi Sim Island.",
    info_2:
      "Bai Tu Long bay attraction:  Vung Vieng fishing village, Thien Canh Son cave, Quan Lan Island, Co To Island, Bai Tu Long National Park... and so on.",
    info_3:
      "Lan Ha bay attraction: Cai Beo fishing village, Dark and Light caves, Ba Trai Dao beach, Viet Hai fishing village, Hospital cave…",
    info_4:
      "Activities: cruising, hiking to the caves or the summits, canoeing in Ti Top Beach, kayaking or rowing boat rides to Luon Cave or Cua Van Floating Villages, swimming, fishing...",
  },
  {
    id: 3,
    name: "Ninh Binh",
    description:
      "Ninh Binh is a magnificent natural landscape destination, located approximately 90 kilometers south of Hanoi City Center, with thousands of karst mountains rising over shimmering rice fields and quiet lagoons. Ninh Binh is also widely known for being rich in culture, history, and diverse ecological resources. In a small area, Ninh Binh has various values, from the ruins of a former palace and ancient temples dated back to 11th-century, to 19th- century. Catholic Church in the form of a Vietnam Communal house, or a huge Buddhist pagoda built only ten years ago.",
    info_1:
      "Visit Hoa Lu - the ancient capital of Vietnam in the tenth and eleventh centuries, the temple built in honor of the two imperial dynasties, surrounded by beautiful gardens, different water features and the picturesque old gates against the backdrop of the breathtaking karst mountains.",
    info_2:
      'Join a rowing boat and cave visiting at Tam Coc Bich Dong - the most attractive tourist destination in Ninh Binh known for its famous name "Halong Bay on Land” by limestone mountains, vast rice fields, small rivers that run between and beautiful grotto.',
    info_3:
      "Hike up the Mua Cave - an attractive limestone cave with one of the pagodas on a narrow rocky point and an incredible viewpoint with a spectacular panorama view of the Ngo Dong River and the rice fields.",
    info_4:
      "Go bamboo boating at Trang An Complex - a UNESCO World Heritage Site, combines virgin mountains and blue water, numerous caves in limestone mountains, the wild natures of forest and rivers, and a large system of distinctive caves.",
  },
  {
    id: 4,
    name: "Sapa",
    description:
      "Homeland of hill tribe people, breathtaking nature landscape with mountain ranges, terrace paddy fields, beautiful valleys and waterfalls. Go trekking and explore Sapa with its magnificent rice terraces, which are still farmed by local ethnic people today as they have been for centuries. Enjoy a cable car ride to the highest peak in the region, Mount Fansipan, at 3143 m above sea level, which crowns the ragged ridge line high above Sapa town.",
    info_1:
      "Outdoor activities: Trekking in local villages and valleys with terrace fields, Mountains conquering at Ham Rong, Fansipan (with cable cab); cycling and moto biking",
    info_2:
      "Nature site: Silver waterfall, Love waterfall (combine with hiking), Heaven Gate",
    info_3:
      "Local experience: Homestay, weekly markets (Bac Ha, Can Cau,…), street food",
    info_4: "",
  },
  {
    id: 5,
    name: "Pu Luong",
    description:
      "A peaceful and little-known place for a perfect retreat with the cascading terrace rice fields, charming hidden villages, and good-natured people. Take an escape to Puluong if you prefer a more peaceful and little-known place for a perfect nature retreat with cascading terraced rice fields, charming hidden villages, and good-natured people. Trekking and cycling are popular activities in Puluong, as are visits to waterfalls, water wheels, and rice terraces.",
    info_1: "Outdoor activities: Trekking and Cycling around ethnic villages",
    info_2:
      "Nature site and landscape: Hieu Waterfalls, water wheels, rice terraces, homestay",
    info_3:
      "Local experience: homestay, panoramic bungalow, craft workshop, farm-to-table home cooking, local market",
    info_4: "",
  },
  {
    id: 6,
    name: "Hue",
    description:
      "Known widely as the former royal capital in the 19th century, Hue, a Unesco site since 1994, reflects the old time’s delicacy and conservation with valuable vestiges from the last feudal dynasty, many ancient relics, archeological traces and a poetic, slow-paced rhythm. Despite only 2hrs from the sunny beaches and lively quarters of Danang and Hoi An, Hue is still quite different with special tranquil ambience that requires you more time to intermingle with the elegant people, to taste their plentiful dishes, to contemplate royal food décor and visit royal heritages’ ornaments, or just to bike thru quiet gardens and villages. For the Vietnamese, Hue is additionally known as a homeland of beloved musicians and artists, whose art masterpieces are found everywhere in the city.",
    info_1:
      "Royal Legacies: The Royal Citadel Complex (UNESCO heritage) & Kings’ tombs (Khai Dinh, Minh Mang, Tu Duc), Hue Royal Court Music (UNESCO intangible heritage)",
    info_2: "Buddhist roots: Thien Mu Pagoda, Tu Hieu Monastery",
    info_3: "Nature: Bach Ma National Park, Tam Giang Lagoon, Lang Co Beach.",
    info_4:
      "Others: Cyclo or cycling, dragon boat on Huong River, street food, royal dining, cooking tour.",
  },
  {
    id: 7,
    name: "Da Nang",
    description:
      "A family friendly destination offering various activities like beach relaxation, amusement parks, world-class casinos, golf courses and entertainment centers; a home of numerous luxury properties. The cluster of Da Nang and its surroundings is growing gloriously with new direct international flights, a deep seaport for cruise ships, good travel infrastructure, and highly qualified services. Da Nang is well-known for its My Khe and Non Nuoc beaches, which stretch for dozens of kilometers and feature crystal water, long sand dunes, and deluxe resorts with spa centers. Da Nang is now a symbol of peaceful Vietnam, offering sunny vacations.",
    info_1:
      "Nature: Marble mountains, Son Tra Peninsula, beaches, Hai Van Pass.",
    info_2: "Museums: Cham Sculpture, Da Nang Art.",
    info_3:
      "Entertainment: Ba Na Hills & Golden Bridge, Vinpearl Da Nang, Sky36 Bar, Love Bridge.",
    info_4:
      "Others: local food, seafood, water sport activities (kite surfing, jet skiing, yachting).",
  },
  {
    id: 8,
    name: "Hoi An",
    description:
      "Once a trading port dating back to the 14th Century, Hoi An old town is famous for its classical beauty, narrow winding yellow-themed streets, and lantern-lit nights, traditional silk-making products and one-day tailor-made clothing. Different from Hanoi or Ho Chi Minh City, Hoi An is small and provincial in feel. It’s a UNESCO World Heritage Site that provides a fascinating glimpse into what port life was like in 18th century Vietnam. Hoi An Vietnam, gives tourists a special impression thanks to a lot of things: the peace of the ancient town, the rustic beauty of craft villages, the wilderness of beaches, and the delicious dishes with memorable flavors.",
    info_1: "Nature: Cu Lao Island, beaches",
    info_2: "Culture: Hoi An Ancient Town, My Son Sanctuary.",
    info_3:
      "Local Experience: Thanh Ha pottery, Cam Thanh coconut, Kim Bong carpentry, cooking class and farmer experience in Tra Que , cyclo and biking, boating activities, lantern making, night market, street food",
    info_4: "Show: Hoian Memories and Hoian Theme Park",
  },
  {
    id: 9,
    name: "Nha Trang",
    description:
      "Proud of sunny white sand beaches, water sports, islands and seabed activities, Nha Trang is also home of many luxurious resorts, amusement parks, golf courses, and world-class restaurants and bars. Perched on a pristine stretch of the southern coast, Nha Trang is a playground for sun seekers. Days here are spent dining on delicious seafood, snorkeling around stunning islands, and partying on the sand after dark. Nha Trang lays claim to some of the country's finest luxury resorts and thrilling watersports. Despite the development boom, colorful fishing villages and serene riverside restaurants are just a stone's throw away.",
    info_1: "Nature: numerous white sand Beaches and crystal water islands.",
    info_2: "Culture: Long Son Pagoda, Bao Dai Villa, Po Nagar Cham Towers.",
    info_3:
      "Entertainment: Sailing Club, Vinpearl Water Park, Sky-light rooftop",
    info_4:
      "Others: mud baths and hot springs, parasailing, scuba diving, snorkeling, sea food.",
  },
  {
    id: 10,
    name: "Da Lat",
    description:
      "Hidden away in the Central Highlands, Da Lat is the cooler cousin to Vietnam's seaside destinations. Famous for its countryside charm, Da Lat draws couples, wellness seekers, and outdoor enthusiasts. This mountain resort town was once a summer getaway for the French, who left their mark in the European-inspired architecture, countless lakes, colorful flower gardens, cascading waterfalls, poetic valleys, winding passes that attract all the romantic people. Da Lat's main lake is the center of the action, but amazing natural wonders wait in the hills all around.",
    info_1:
      "Culture: Bao Dai Palaces, Da Lat Railway Station, Crazy House, Linh Phuoc Pagoda, Truc Lam Monastery.",
    info_2:
      "Nature: Xuan Huong lake, Da Lat flower garden, Love Valley, Falls (Datanla, Elephant), Lang Biang Mountain, Bidoup Nui Ba National Park.",
    info_3:
      "Outdoor activities: Canyoning, white water rafting, mountain biking/hiking, jungle trekking, high rope course, jeep.",
    info_4: "",
  },
  {
    id: 11,
    name: "Mui Ne",
    description:
      "Taking about a 4-hour drive from Ho Chi Minh City, this is an ideal place for both water sports enthusiasts and more relaxed beach-goers. Known as Vietnam’s Sahara Desert, sand dunes offer many funny games including jeep drive, sand sledding, and squad bike.",
    info_1:
      "Nature: Mui Ne beaches, Mui Ne Harbour, fishing village,  fairy springs, white/red sand dunes.",
    info_2: "Culture: Po Shanu Cham Towers.",
    info_3:
      "Outdoor activities: Surfing, Kite surfing, jeep drive, sand sledding, and squad bike, golf",
    info_4: "",
  },
  {
    id: 12,
    name: "Ho Chi Minh",
    description:
      'Saigon is the largest metropolis in Indochina, a harmony of French buildings and churches, shadowy streets, enthralling local-style markets, and modern skyscrapers. Saigon, more than any other city in Indochina, is a melting pot of religions, ranging from Buddhism, Catholicism, Protestantism, Islam, and Hinduism to local religions "Cao Dai" and "Hoa Hao," resulting in distinct colors in each quarter. Connected with Cambodia, Thailand, Singapore, Malaysia, and Indonesia by short air, river, and land routes, Saigon has quickly become the first place for innovation, creativity, and new tour experiments in Vietnam. The city is also becoming a good transit point for the best beaches in Asia, including Con Dao and Phu Quoc islands and other beaches on the southern coastline.',
    info_1:
      "Culture and history: Reunification Palace, War Remnant Museum, Ben Thanh Market, Jade Emperor Pagoda, Cu Chi Tunnels.",
    info_2:
      "Colonial French architecture: Notre Dame Cathedral, Central Post Office, Saigon Opera House, City Hall or Hotel de Ville.",
    info_3:
      "Chinese Quarter: Binh Tay Market, Thien Hau Pagoda, Chaozhou Assembly Hall, Cho Lon Mosque.",
    info_4:
      "Activities: cyclo tour, street food, city jeep tour, walking tour, vespa/minsk motorbike tour, cruise dinner on Saigon river.",
  },
  {
    id: 13,
    name: "Mekong Delta",
    description:
      'To escape the hustle and bustle of urban city life and seek tranquility, the Mekong Delta is a good choice. This delta is well-known as the "rice bowl" of Vietnam, making the country the top 5th world exporter of rice. This land is a water world, comprising low-lying paddies and rivers bordered by dense mangroves and palms, with famous early-morning floating markets. Due to the flat topography, the best tours are still the "Mekong delta trips" to Vinh Long, Can Tho, Sa Dec, Tra Vinh, and especially Chau Doc, the border town, from where we can get on a deluxe river cruise linking Vietnam and Cambodia, enjoying high-class services and relaxation on the Mekong.',
    info_1:
      "Boat trip: day trip and overnight trip within Vietnam and between Vietnam and Cambodia",
    info_2: "Floating market: Cai Be, Cai Rang, Phong Dien",
    info_3: "Forests: Tra Su Mangrove Forest, Tram Chim National Park",
    info_4:
      "Excursion: visit local family-run factories, orchards and tasting fruits, listen to folk music, Binh Thuy Ancient House, Sam Mountain, Vinh Trang Pagoda.",
  },
  {
    id: 14,
    name: "Phu Quoc",
    description:
      "Located in the Gulf of Thailand, Phu Quoc is recognized as the largest island in Vietnam, an visa-exempt isolated island famous for white beaches, crystal clear water and colorful coral reefs. The pearl island possesses a variety of beautiful beaches stretching from the north to the south. Ninety-nine mountains, majestic hills, and primeval forests with diverse flora and fauna feature the unique beauty of Phu Quoc Island. If you travel to the north of the island, you will have the opportunity to visit some famous destinations such as Rach Vem Fishing Village, Hon Mot Island, Long Beach, VinWonders Phu Quoc, Vinpearl Safari Phu Quoc, and so on. Meanwhile, in the south, the 12 islands of various sizes located in the An Thoi Archipelago will be an ideal stopover for nature exploration activities.",
    info_1:
      "Nature: Beaches & Islands, Phu Quoc National park, Sim forest, pearl farm.",
    info_2:
      "Culture & History: Dinh Cau Temple, Coconut Tree Prison, Cua Can Village.",
    info_3:
      "Entertainment: Theme Parks, Safari,  Casinos and shopping mall, Golf.",
    info_4:
      "Water sports: snorkeling, scuba diving, speedboats, canoeing, yachting, seabed walking.",
  },
  {
    id: 15,
    name: "SIEM REAP",
    description:
      "People only know about ‘’the Angkor’’ when they are in Siem Reap. Obviously, there is nothing like this masterpiece of architecture and sculpture in the world, it has been voted as one of the new wonders of the modern world at different polls.Less than 10km from the center of Siem Reap, Angkor consists of hundreds of temples, shrines, small vestiges, artificial lakes, stone towers, and many of them still carry the remaining traces of the rainforest or jungle from the old time. Being the life-support system for the Angkor, Siem Reap itself is the epicenter of the new Cambodia, an alive and popular place that is rising up speedily. Siem Reap is still a small, charming town, with old French shop-houses, shady tree-lined boulevards and a slow-flow river, where you can board a boat trip to see a part of the Great Lake of Tonle Sap - a pride and significant living source of Cambodia.",
    info_1:
      "Temples: Angkor Complex, Wat Bo, Wat Thmei (killing fields), Wat Damnak, Wat Preah Prom Rath,…",
    info_2:
      "Boat trip to visit floating villages on Tonle Sap lake: Kompong Khleang, Kompong Phluk,",
    info_3:
      "Siem Reap craft workshops: Les Artisans d'Angkor, Senteurs d’Angkor, Puok silk village",
    info_4:
      "Others: Hot Air Balloon, Cooking Class, Horse riding, elephant riding, Gondola cruise dinner, Zipline in Jungle.",
  },
  {
    id: 16,
    name: "BATTAMBANG",
    description:
      "A laidback town is different from a hustling Phnom Penh or tourist crowded Siem Reap.  This is the home of bamboo trains and numerous French colonial architectures including administrative buildings, shop-houses, apartment buildings, …It has a growing number of ex-pats fueling the growth of artsy cafes, restaurants and bars.  The main draw in Battambang is the city’s time-warped collection of colonial architecture, a lazy riverside ambience with some interesting day-trips around town – including fun countryside rides on the quirky bamboo railway. The surrounding province once had more temples than Siem Reap, although none was on the scale of Angkor Wat and most have long disappeared. Some remains that are worth a visit are the hilltop site of Wat Banan and the nearby mountain and temple complex of Phnom Sampeu.",
    info_1:
      "Battambang Bat Caves,  Ta Dumbong Statue - a symbol of the city.",
    info_2:
      "Crocodile Farm, reserved area at Prek Toal bird sanctuary.",
    info_3:
      "Provincial Museum - an excellent display of sculptures and sandstone carvings.",
    info_4:
      "Show: Phare Ponleu Selpak.",
  },
  {
    id: 17,
    name: "PREAH VIHEAR",
    description:
      "Preah Vihear province borders Thailand and Laos to the North – a vast province known as the “mother of mountain temples' ', home to Dangrek Mountain range forming the natural border between Cambodia and Thailand, famous with the sacred Preah Vihear temple, an ancient Hindu temple. Prasat Preah Vihear is the most famous in the province, with a breathtaking view of the Cambodian countryside and Phnom Kulen, a famous mountain, north of Siem Reap on the horizon. Preah Kan, which covers almost 5 square kilometers, is the largest temple enclosure built during the Angkorian era, in the 9th century. Moreover, Koh Ker was the former capital of the Angkorian Empire.",
    info_1:
      "Prasat Preah Vihear temple; Koh Ker temple – a former 10th century capital of the Khmer Empire",
    info_2:
      "The religious complex of Preah Khan Kompong Svay",
    info_3:
      "Thmatboey village located in Kulen Promtep Wildlife Sanctuary, the only known nesting site of the endangered Giant Ibis and White-shouldered Ibis.",
    info_4:
      "",
  },
  {
    id: 18,
    name: "KAMPONG THOM",
    description:
      "Meaning 'great port' in Khmer,  Kampong Thom is known for many beautiful Angkorian temples and also popular among the tourists for the Tonle Sap biosphere, including the lake and boating tours.Being known as big provinces of the Kingdom of Cambodia, Kampong Thom and Preah Vihear are both next to Siem Reap and have big potentials for tourism development not only by location but also by their history, nature and culture. They are mainly about temple visits but who are interested in remote villages, Preah Vihear & Kampong Thom also can match the demand. ",
    info_1:
      "Sambor Prei Kuk – an archaeological site known for the pre-Angkorian ruins, which are clusters of temple complexes located in the premises, surrounded by forests.",
    info_2:
      "Boating tours in the Tonle Sap – explore the fishing villages, mangrove forests and the rich biosphere on slow boats.",
    info_3:
      "Phnom Santuk – a sacred mountain, home to reclining Buddha and is known for the fantastic views of the Tonle Sap reserve.",
    info_4:
      "Prasat Kuh Nokor – an Angkorian Buddhist temple known for its two ponds.",
  },
  {
    id: 19,
    name: "KAMPONG CHAM",
    description:
      "Located in the East of the country where the Mekong river runs along, the city name means “the port of Cham people”, home to a minor group living here for a long time who follow Muslim together with Christians and ethnic Chinese backgrounds. This quiet provincial capital retains an air of faded colonial gentility.",
    info_1:
      "Religious Sites: Nokor Wat (an Angkorian temple dating back to the 11th century); Nokor Bachey Pagoda (an ancient temple known for its simple yet elegant architecture); Sambo Prei Kuh (used as the ancient capital of Chenla). Sunrise at Phnom Hanchey Hilltop Temple.",
    info_2:
      "The King’s residence which served as home for the ruling monarchs.",
    info_3:
      " Kizuna Bridge (the first bridge built over the Mekong River in Cambodia) and Bamboo Bridge",
    info_4:
      "Koh Paen island,  Phnom Srey and Phnom Pros Mountain,   old French Lighthouse, cycling tour to the Chup Rubber Plantation.",
  },
  {
    id: 20,
    name: "KRATIE",
    description:
      "Further north along the Mekong, Kratie is another old French-era settlement, best known for the rare Irrawaddy dolphins that inhabit the nearby rapids at Kampie. For nature, eco-tourism and wildlife lovers, the trip to Kratie is a must. As you wind deeper and deeper into the ‘real’ Cambodia – the Cambodia of rice fields, lotus farms and sleepy wooden villages – the chaos of the capital just melts away.",
    info_1:
      "Sunset Boat Tour on the Mekong River and see the life of local fishermen and the endangered Irrawaddy Dolphins.",
    info_2:
      "Koh Trong Island – an ecotourism paradise with picturesque landscape and peaceful, laidback life of the native.",
    info_3:
      "Religious sites: Vihear Sarsar Mouy Rouy (100-column pagoda); Phnom Sombok Buddhist hill temple, Wat Roka Kandal Sanctuary: The Snuol Wildlife Sanctuary; Phnom Preah – home to many species of wildlife including a vast range of birds and mammals; Mekong Turtle Conservation Centre (MTCC) – an establishment for the conservation of the endangered Cantor’s Giant Softshell Turtle.",
    info_4:
      "Others: Waterfall of Cham Pey, basket weaver villages.",
  },
  {
    id: 21,
    name: "PHNOM PENH",
    description:
      "Featured by well-organized streets, charming old houses alongside new deluxe buildings, busy river ports, exciting night markets and various cuisines, Phnom Penh is a typical growing Asian capital. Owning the biggest international airport of the country and connecting with Vietnam by short landway and riverway, Phnom Penh welcomes not only tourists coming for holiday but the businessmen from Vietnam, all over Asia, and beyond. In comparison with other towns in Cambodia, Phnom Penh resembles the fascination and growth of the country utmost. On the other hand, the city will relieve you with its deep cultural value and inspiring architectural works.",
    info_1:
      "Culture site: Royal Palace complex with Silver Pagoda; The Independence Monument; The Statue of King Father Norodom Sihanouk",
    info_2:
      "National Museum – the largest and the central museum of the country,  Tuol Sleng Genocide Museum; Choeung Ek Genocidal centre (Killing Fields)",
    info_3:
      "Wat Phnom – located on the top of the city’s hill, Koh Dach (Silk Island); Tonle Bati; A sunset boat tour on the Mekong river.",
    info_4:
      "Others: Mekong Island cycling tour, Beer Tasting Experience, foodie tour,",
  },
  {
    id: 22,
    name: "SIHANOUKVILLE",
    description:
      "A coastal town owning a series of magnificent beaches, restaurants serving delicious seafood cuisines and a happening nightlife making it a popular destination for a beach holiday. Sihanoukville is also the lead center of trading, economy and beach tourism of the country.",
    info_1:
      "Numerous white sand beaches with seafood restaurants and café shops.",
    info_2:
      "Water sports including kayaking, snorkeling, scuba diving, swimming and standup paddle boarding.",
    info_3:
      "Nature site: Ream National Park; Kbal Chhay Waterfall",
    info_4:
      "Bamboo Island – a popular getaway for a slice of secluded white sand beaches surrounded by mangrove forests, which is home to many wildlife species of animals and birds.",
  },
  {
    id: 23,
    name: "KOH RONG",
    description:
      "An offshore island called Maldives of Cambodia, home to the best luxury resorts and crystal clear blue water beaches. This island belongs to Koh Rong province. Its neighbor is Koh Rong Saloem Island, together both islands contribute to Cambodia incoming by their beach tourism. You can book a resort in one of them and enjoy your holiday, get away from urban noise.",
    info_1:
      "Relax at the main beaches: The Lazy Beach, Coconut Beach, Southwestern Beach, Long Set Beach, and Saracen Bay Beach.",
    info_2:
      "Scuba diving or Deep-sea diving at Koh Rong Sanloem, Koh Tang and Koh Prins islands.",
    info_3:
      "Bioluminescent plankton – produce green, golden and other colors, which glow in the night when they come near the seashore.",
    info_4:
      "Sok San fishing village – offers authentic Cambodian Khmer village experience of getting to know the fishing families.",
  },
  {
    id: 24,
    name: "LUANG PRABANG",
    description:
      "Luang Prabang is a must to see! Being one of the former ancient capitals of Laos, Luang Prabang (or Muong Luang) is an incredibly graceful boutique town with authentic Laos spirit, traditional old houses, French-styled villas adjacent to lavish ancient temples and shrines. Luang Prabang is also a river-cross area, where you can witness many Boat races in high water time, visit gorgeous pagodas in the countryside, and wake up at five o’clock to see hundreds of Theravada Buddhist monks in their daily alms giving ritual. Mountain ranges encircle the city in lush greenery. People love to spend several days here by biking around shadowed streets, peaceful markets full of goods, foods and tropical aroma, where bikes do not need being locked, then off to visit the Asia Center of Elephants Feeding.",
    info_1:
      "Join Morning Alms Givings: A longstanding tradition in Laos Buddhist culture and a sacred ceremony for the locals and the monks",
    info_2:
      "Climb to the top of Phousi Mountain; hiking, swimming and soaking up with the natural beauty of the surrounds of Kuang Si Waterfall",
    info_3:
      "A river boat trip to the Pak Ou caves - the most revered holy sites in Laos, a pleasant boat ride, combining a visit to the culturally important caves with a day out in superb riverside landscape and local villages.",
    info_4:
      "Visit Handicraft Villages: Ban Xang Hai – local wine making, Ban Phanom – hand weaving, Ban Xang lek – Saa paper making, Silk weaving and some of H’mong Village famous for bamboo weaving.",
  },
  {
    id: 25,
    name: "VIENTIANE",
    description:
      "Located in the Northwest of Laos, Vientiane is the capital as well as the largest city of Laos. Compared to the hectic, bustling capitals in other Southeast Asian countries, Vientiane is known for its laid back atmosphere. The city presents a valuable, long-standing Laos with ancient, golden relics, mystic stories related to Buddhism and Hinduism, historic religious beliefs and mixed Lao-France architecture with Laos sculptures. It’s the most attractive destination for those who enjoy a peaceful, relaxed vibe. Even though in Vientiane the days blend into one another, once you leave, you’ll miss this place more than you expected. Just apart from Thailand by a bridge over Mekong River, Vientiane is still a border-gate, from where you will leave behind the developed, hectic, modernized 21st-century world to deep yourself into a laid-back atmosphere and slow pace of life that you may have always wished for. With very few traffic jams, noise or rush hours, Vientiane presents a valuable, long-standing Laos with golden relics, solemn pagodas, and crowded but elegant streets and boulevards.",
    info_1:
      "Visit Theravada pagodas; attend annual Pha That Luang Festival in October.",
    info_2:
      "Take Vientiane as the departure to the elephant feeding center at Phu Khao Khuay National Protected area, or to Vang Vieng and the Plain of Jars.",
    info_3:
      "Walking, biking or taking tuk tuk to enjoy the peaceful atmosphere around the city and visit the central temples.",
    info_4:
      "Taking place at dyes class or visiting Lao Textiles workshop, studio and gallery creates woven art.",
  },
  {
    id: 26,
    name: "VANG VIENG",
    description:
      "Located only around 4 hours driving to the north of Vientiane, Vang Vieng is not just a bus stop on the long journey up to Luang Prabang, but also is a real tourism destination in its own right. The town lies on the western side of Nam Song River with an incredible view to the river with the karst hill landscape as the background. Surrounded by pastoral fields populated by water buffaloes, several caves, viewpoints and a series of so-called Blue Lagoons-- turquoise waterholes, the town has been known as a party destination for years. Today, more outdoor-oriented activities are now available such as trekking, kayaking, caving, zip lining, swimming, rock climbing and hot air balloon. The town is also called the “tubbing capital of the world”.",
    info_1:
      "Hiking at two of the best mountains of Vang Vieng are Phangern and Nam Xay – both viewpoints in Vang Vieng offer spectacular panoramic views and are relatively beginner-friendly hikes.",
    info_2:
      "Kayaking on Nam Song river:  Pick a single or double kayak and choose your pace. Paddling is hardly necessary as you float with the current down the Nam Song.",
    info_3:
      "Zipline near this area: This is a fairly new activity in the region and you can travel to Tham Nam, better known as the Water Cave and then whizz through the air on a zip line.",
    info_4:
      "Swim in lagoons: The most well-known hot spot in Vang Vieng is the Blue Lagoon, a swimming spot famous for its aquamarine water… and is usually crowded. There are 3 that are all named “The Blue Lagoon” and more can be found out of Vang Vieng.",
  },
  {
    id: 27,
    name: "XIENG KHOANG",
    description:
      "Lies in the northeast of Laos near the Laos-Vietnam border, the province best known for the mysterious Plain of Jars site near Phonsavan, the provincial capital. Besides this historical site, there are beautiful waterfalls and trekking opportunities nearby, and the chance to learn about how the war affected the people at the UXO Centre.",
    info_1:
      "Plain of Jars: hundreds of giant jars mostly made of sedimentary rock interspersed throughout the Xieng Khouang plain. The stone jars of all sizes strewn all over the plateau make an awesome sight. The jars are ranging from 1 to 3 meters; each can weigh up to 14 tons.",
    info_2:
      "Visit Moung Khoune- the ancient Tai Phuan Kingdom once prospered as a trade center around the 12th century and later became a vassal of the Lao Lan Xang Kingdom and  Wat Phia Wat temple - another remaining site during the ravages of historical war.",
    info_3:
      "Soak in Hot Springs at Baw Nyai which is some 67 kilometers outside of Phonsavan and this is a great place to relax and soak in a hot tub from the jungle.",
    info_4:
      " Attempt a challenging 3 to 4-hour hike along the Mat river valley and up Xang Mountain to Plain of Jars Site 42. Then come back down for a refreshing dip in the pools that are a mixture of the hot springs and cooler water from the Mat River, which results in a perfect temperature for swimming.",
  },
  {
    id: 28,
    name: "SOUTHERN LAOS",
    description:
      "This area is a riverine archipelago with the most islands in Indochina. Southern Laos not only has beautiful scenery but also attracts many tourists thanks to the unique location. A perfect place to stop if we travel from Laos to Cambodia by land or the other way round. Famous for being the habitat of Irawaddy dolphins, the area also has an abundance of birdlife, beautiful waterfalls and dramatic mountain ranges. Visit the Khone Phapheng falls, the largest falls in Southeast Asia and the reason boats cannot transverse the Mekong to China. On top of all that, the region boasts the impressive Khmer temple complex of Wat Phou and the Bolaven Plateau, home to the best coffee in Laos. Pakse is considered the gateway to southern Laos, where highlights range from Si Phan Don (4000 Islands), to ancient Wat Phou, and the majestic Bolaven Plateau.",
    info_1:
      "Si Phan Don - or “Four Thousand Islands” - The riverine network carries us on a journey of extreme natural beauty, deeply embedded traditions, vibrant cultures, and opportunities for some exhilarating activities.",
    info_2:
      "Wat Phou: An imposing reminder of the Angkorian empire that once dominated much of Southeast Asia, Wat Phou is one of the most impressive Khmer ruins outside Cambodia.",
    info_3:
      "Bolaven Plateau: One of the most beautiful areas in Laos, the region of stunning waterfalls, tribal villages and traditional coffee and tea plantations.",
    info_4:
      "Go on a Kayaking Trip to see Irrawaddy Dolphins: beside the normal boat rise, kayaking is a great way to explore the hidden waterways of the Mekong, see water buffalo, marvel at some beautiful waterfalls including Li Pi Falls and Khone Phapheng Falls, and glimpse the endangered Irrawaddy Dolphins.",
  },
  {
    id: 29,
    name: "YANGON",
    description:
      "Yangon is an attractive city, though it is not the capital anymore, it still plays the most important role in tourism development as the hub of incoming flights and the heart of ancient culture and longest traditions, as well as a center of new development. The city is also recorded for independent spirit, young talents, and consecutive efforts toward a more civilized society. On the growth, tourists to Yangon have been booming recently because of its thousand-year golden pagodas, ancient temples and churches of various religions, charming old houses and hotels since the 19th century. Recently, the deluxe night buses and trains tours connect the city with Bagan, Mandalay, Inle Lake and so on, now save your cost and give you more local experience. Yangon is indeed making tourists feel at home and has many selections for their tours, hence every visitor has planned to come back here to catch up with new enjoyable changes of a growing country.",
    info_1:
      "Visits to 2,600-year-old Shweddagon pagoda; learning about pearls at the Gem Museum; shopping at Scott Market and saving time for photo tour and meditation.",
    info_2:
      "Departure for at least two-week tour to Bagan, Salay, Mandalay, Inle Lake on the 1st visit, using riverway, airway or landway.",
    info_3:
      "Departure to visit Kayin States and Mon States in the Southeast on the 2st visit; visiting mysterious Mrak-ou, Ngapali beach or other beaches.",
    info_4:
      "Land transfer to the Southeast to Kyaiktiyo, Hpa-an, Mawlamine; going North for long treks in Kachin state.",
  },
  {
    id: 30,
    name: "BAGAN",
    description:
      "Most tourists will plan to visit both of these treasures of Myanmar, and the connection between the two cities is easy and convenient now not only by air. Stretching over 41,000sq km with 4,000 thousand relics, BAGAN is an irresistible attraction, where every temple or stupa is a unique piece of art and belongs to a different time, so the masters had made them diverse with their own creativeness. At sunrises and sunsets, visitors can contemplate the sun hanging over a horizon, prolonging above thousands of stupas and temples; many tourists will also wait for the high season to book an expensive balloon to get the best view here.",
    info_1:
      "Using bikes or horse-carts instead of vehicles in order to keep the area clean, quiet and not to scare the locals’ cattle.",
    info_2:
      "Staying at a hotel near the river, enjoying local food with the locals, strolling around Nyang-U and visiting Swezigon pagoda at sunset time.",
    info_3:
      "Giving jobs to the locals by traveling to/from Mandalay via night deluxe bus, or riverway when possible, instead of flights.",
    info_4:
      "Great place for meditation, religious tours, education tours and theme tours about fine-arts.",
  },
  {
    id: 31,
    name: "MANDALAY",
    description:
      "Coming to MANDALAY is different, the city is a strange combination of old and new things. From low-paced Bagan up to Mandalay seems like a back step to the modern world you have escaped, with the exciting ambience of the big communities of Chinese, Muslim, Burmese. They make Mandalay not only more exciting and business-looking, but also more difficult for tourists to comprehend. Beside the great vestiges, this city is also a good stop for shopping for hi-qualified silver or crafts. In a word, Mandalay gives you the feelings of a growing Myanmar, whilst protecting its ancient values amazingly.",
    info_1:
      "Visits to Amarapura to contemplate the incomparable sunset at long U-bein teak wood bridge and monastery; taking a boat trip to view U-bein bridge and fields from another side.",
    info_2:
      "Sitting in meditation at Mahamuni Paya pagoda; visiting Sagaing and Mandalay Hills with great temples, pagodas and panorama over the surroundings.",
    info_3:
      "Departure to Pyin Oo Lin hill resort where you can connect to ASEAN highway in near future; from Pyin Oo Lin, take the sitting train to go by a viaduct built in 1901 to Hsipaw with great view of deep valley and abyss.",
    info_4:
      "Taking a boat trip from Mandalay to Mingun within a day, and from Mandalay to Bagan and Yangon.",
  },
  {
    id: 32,
    name: "INLE LAKE AND KALAW TREK",
    description:
      "Leg-rowing fishing boats, blue crystal water merging with deep blue sky, workshops and gardens, you are at the Inle Lake of Myanmar! Official data mentions that the lake is about 116 - 120sq km, yet it hardly defines the lake’s boundary due to the high water level and thick aquatic plants growing around the shore. A boat trip here is a great leisure to the souls and eyes, besides unique leg-rowing boats, you will be surprised by lively colors – blue, pink, green, purple - the lakemen use to paint their houses and boats. In the Southwest of the lake is Indein, a great hub of hill trekking, where many families have become trekking guides, along with whom you can spend several days in rural villages, visiting ancient monasteries in the forest or going through local green gardens and textile hamlets. The Lake and its surrounding are not only sightseeing places but also a little world, where you can enjoy the real life of Myanmar.",
    info_1:
      "Photography tours all year round; visiting in September, when lotuses bloom; visiting in October during the leg-rowing boat race.",
    info_2:
      "Boat trip at Inle Lake, enjoying the pass-by floating gardens of cucumber, bean and tomato, visiting and shopping at silver, silk and cigarette workshop",
    info_3:
      "Trekking in Kalaw hills, Indein and Nwangshwe.",
    info_4:
      "Staying at a lake deluxe resort for several days.",
  },
  {
    id: 33,
    name: "GOLDEN ROCK AND THE SOUTHEAST MYANMAR",
    description:
      "As prominent as the Angkor to the Khmer, the Golden Rock defines what we are saying about Myanmar. Located on Kyaiktiyo Mountain, the rock has become the top Buddhist pilgrimage site since the 11th century. In holidays, devotees from Myanmar and neighboring countries flock to the site to plate gold leaves on the surface of the big rock, on which Kyaiktiyo Pagoda is built. From the Rock, convenient roads lead you further Southeast to less discovered Mon and Kayin states, where you can spend three or four days at charming Hpa-an, climbing up it's incredibly beautiful Zwekabin peak to enjoy the view of Lumbini garden with hundreds of Buddha statues, to visit a grandiose cave wall with carving masterpiece, or transfer to Mawlamyine, a very graceful but hectic coastline town. When Bagan and Mandalay give us respect to ancient Myanmar, the Golden Rock and the Southeast area take us closer to the normal life of people within surprising discoveries.",
    info_1:
      "Five to six-day tour, including visiting the temple at Kyaiktiyo (the Golden Rock Temple), Hpa-an and then Mawlamyine.",
    info_2:
      "Visiting charming Hpa-an’s Kaw Gun Cave with exceptional carvings, climbing Zwekabin Mountain.Koh Trong Island – an ecotourism paradise with picturesque landscape and peaceful, laidback life of the native",
    info_3:
      "Boat trip from Hpa-an and Mawlamyine, visiting its wonderful old colonial town, enjoying the sunset and seabirds at the port opposite Strand hotel, then tasting great seafood at Mawlamyine.",
    info_4:
      "Visits to ancient monasteries and pagodas with great views at Mawlamyine.",
  },
];

export const cityNames = cities.map((city) => city.name);
