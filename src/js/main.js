import { cities } from "./constant.js";
import { cityNames } from "./constant.js";

(function ($) {
  $(".dropdown-toggle").click(function (e) {
    if ($(document).width() > 991) {
      e.preventDefault();

      var url = $(this).attr("href");

      if (url !== "#") {
        window.location.href = url;
      }
    }
  });
})(jQuery);

$(window).scroll(function () {
  var scroll = $(window).scrollTop();

  //>=, not <=
  if (scroll >= 500) {
    //clearHeader, not clearheader - caps H
    $(".travel-menu").addClass("fixed-menu");
  } else {
    $(".travel-menu").removeClass("fixed-menu");
  }
});

$(".btn-search-top").click(function () {
  $(".form-search-top").toggleClass("show");
});

$(".viet-nam__weather .weather-tab").click(function () {
  $(".viet-nam__weather .weather-tab").addClass("active");
  $(".viet-nam__weather .quick-facts-tab").removeClass("active");
  $(".viet-nam__weather .faq-tab").removeClass("active");

  $(".viet-nam__weather--content .weather").addClass("d-block");
  $(".viet-nam__weather--content .quick-facts").removeClass("d-block");
  $(".viet-nam__weather--content .faq").removeClass("d-block");
});

$(".viet-nam__weather .quick-facts-tab").click(function () {
  $(".viet-nam__weather .quick-facts-tab").addClass("active");
  $(".viet-nam__weather .weather-tab").removeClass("active");
  $(".viet-nam__weather .faq-tab").removeClass("active");

  $(".viet-nam__weather--content .quick-facts").addClass("d-block");
  $(".viet-nam__weather--content .weather").removeClass("d-block");
  $(".viet-nam__weather--content .faq").removeClass("d-block");
});

$(".viet-nam__weather .faq-tab").click(function () {
  $(".viet-nam__weather .faq-tab").addClass("active");
  $(".viet-nam__weather .quick-facts-tab").removeClass("active");
  $(".viet-nam__weather .weather-tab").removeClass("active");

  $(".viet-nam__weather--content .faq").addClass("d-block");
  $(".viet-nam__weather--content .quick-facts").removeClass("d-block");
  $(".viet-nam__weather--content .weather").removeClass("d-block");
});

for (let i = 1; i <= 7; i++) {
  $(`.viet-nam__weather .faq .pagination .p${i}`).click(function () {
    $(`.viet-nam__weather .faq .pagination .p${i}`).addClass("active");
    $(`.viet-nam__weather .faq .item .i${i}`).addClass("d-block");
    for (let j = 1; j <= 7; j++) {
      if (j != i) {
        $(`.viet-nam__weather .faq .pagination .p${j}`).removeClass("active");
        $(`.viet-nam__weather .faq .item .i${j}`).removeClass("d-block");
      }
    }
  });
}

cityNames.forEach((cityName) => {
  $(`.viet-nam__culture map area[title="${cityName}"]`).click(function () {
    const city = cities.find(_city => _city.name === cityName);
    
    if (!city.info_4) {
      $(`.viet-nam__culture--intro .info_4`).addClass("d-none");
    } else {
      $(`.viet-nam__culture--intro .info_4`).removeClass("d-none");
    }

    $(`.viet-nam__culture--intro .title`).text(city.name);
    $(`.viet-nam__culture--intro .description`).text(city.description);
    $(`.viet-nam__culture--intro .info_1 p`).text(city.info_1);
    $(`.viet-nam__culture--intro .info_2 p`).text(city.info_2);
    $(`.viet-nam__culture--intro .info_3 p`).text(city.info_3);
    $(`.viet-nam__culture--intro .info_4 p`).text(city.info_4);
  });
});
